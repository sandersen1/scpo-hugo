+++
title = "abricot chai"
date = 2019-10-20T21:18:00+02:00
tags = ["recipe"]
seasons = ["summer"]
ingredients = ["abricot", "cardamome", "gingembre"]
description = "Nectar d'abricot parfumé à la cardamome"
cover = "https://beendi.com/wp-content/uploads/2016/07/Chai_Chai-Latte-Glace-et-Jus-Abricot-Chai_Ete-5424.jpg"
+++

## Préparation : 5 minutes

## Ingrédients (pour 2 verres) :

- 50 cl de nectar d’abricot
- 2 c. à café de Chai L’Intense
- Glaçons

## Réalisation

- Mélangez le nectar et le chai.
- Versez sur des glaçons et servez.

## Astuce

Pour un parfum de chai encore plus intense, utilisez les Glaçons au Chai !
